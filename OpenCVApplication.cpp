// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include <queue>
#include <random>
#include <fstream>
#include <math.h>



using namespace std;

ifstream f("input.txt"); 
ofstream g("output.txt");

int ok1, ok2;

int dib[] = { 0, -1, -1, -1, 0, 1, 1, 1 };
int djb[] = { 1, 1, 0, -1, -1, -1, 0, 1 };


void testOpenImage()
{
	char fname[MAX_PATH];
	if(openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		imshow("image",src);
		waitKey();
	}
}

void cloneAdd()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				if (val + 100 > 255)
					dst.at<uchar>(i, j) = 255;
				else
				    dst.at<uchar>(i, j) = val+100;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void cloneTh(int th)
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				if (val > th)
					dst.at<uchar>(i, j) = 255;
				else
					dst.at<uchar>(i, j) = 0;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void cloneAddC()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst(src.rows, src.cols, CV_8UC3);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				if (pixel[0] + 100 < 255)
					pixel[0] += 100;
				else
					pixel[0] = 255;
				dst.at<Vec3b>(i, j)= pixel;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void cloneSubC()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst(src.rows, src.cols, CV_8UC3);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				if (pixel[2] - 100 > 0)
					pixel[2] -= 100;
				else
					pixel[2] = 0;
				dst.at<Vec3b>(i, j) = pixel;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void cloneMono()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst1(src.rows, src.cols, CV_8UC3);
		Mat dst2(src.rows, src.cols, CV_8UC3);
		Mat dst3(src.rows, src.cols, CV_8UC3);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				dst1.at<Vec3b>(i, j)[1] = 128;
				dst1.at<Vec3b>(i, j)[2] = 128;
				dst2.at<Vec3b>(i, j)[0] = 128;
				dst2.at<Vec3b>(i, j)[2] = 128;
				dst3.at<Vec3b>(i, j)[0] = 128;
				dst3.at<Vec3b>(i, j)[1] = 128;
				Vec3b pixel = src.at<Vec3b>(i, j);
				dst1.at<Vec3b>(i, j)[0] = pixel[0];
				dst2.at<Vec3b>(i, j)[1] = pixel[1];
				dst3.at<Vec3b>(i, j)[2] = pixel[2];
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie1", dst1);
		imshow("Destinatie2", dst2);
		imshow("Destinatie3", dst3);
		waitKey();
	}
}

void clone2Grayscale()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst1(src.rows, src.cols, CV_8UC1);
		Mat dst2(src.rows, src.cols, CV_8UC1);
		Mat dst3(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				dst1.at<uchar>(i, j) = pixel[0];
				dst2.at<uchar>(i, j) = pixel[1];
				dst3.at<uchar>(i, j) = pixel[2];
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie1", dst1);
		imshow("Destinatie2", dst2);
		imshow("Destinatie3", dst3);
		waitKey();
	}
}

void rgb2gs(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst1(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				dst1.at<uchar>(i, j) = (pixel[0]+pixel[1]+pixel[2])/3;

			}
		}
		imshow("SURSA", src);
		imshow("Destinatie1", dst1);
		waitKey();
	}
}

void rgb2gsPonder(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst1(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				Vec3b pixel = src.at<Vec3b>(i, j);
				dst1.at<uchar>(i, j) = pixel[0]*0.07 + pixel[1]*0.71 + pixel[2]*0.21;

			}
		}
		imshow("SURSA", src);
		imshow("Destinatie1", dst1);
		waitKey();
	}
}

void cloneSub(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				if (val - 100 < 0)
					dst.at<uchar>(i, j) = 0;
				else
					dst.at<uchar>(i, j) = val - 100;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void cloneMul(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				if (val * 2 > 255)
					dst.at<uchar>(i, j) = 255;
				else
					dst.at<uchar>(i, j) = val * 2;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void cloneNegative(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				
					dst.at<uchar>(i, j) = 255 - val;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void rgb2usv(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src;
		src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat H(src.rows, src.cols, CV_32FC1);
		Mat S(src.rows, src.cols, CV_32FC1);
		Mat V(src.rows, src.cols, CV_32FC1);

		Mat RH(src.rows, src.cols, CV_8UC1);
		Mat RS(src.rows, src.cols, CV_8UC1);
		Mat RV(src.rows, src.cols, CV_8UC1);
		Mat dst(src.rows, src.cols, CV_8UC3);

		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
				Vec3b val = src.at<Vec3b>(i, j);
				float r = val[2] / 255.0;
				float g = val[1] / 255.0;
				float b = val[0] / 255.0;
				float v = max(max(r, g), b);
				float m = min(min(r, g), b);
				float c = v - m;
				V.at<float>(i, j) = v;
				if (v != 0.0)
				{
					S.at<float>(i, j) = c / v;

				}
				else S.at<float>(i, j) = 0.0;
				if (c != 0.0)
				{
					if (v == r) H.at<float>(i, j) = 60.0*(g - b) / c;
					if (v == g)H.at<float>(i, j) = 120.0 + 60.0*(b - r) / c;
					if (v == b) H.at<float>(i, j) = 240.0 + 60.0*(r - g) / c;
				}
				else H.at<float>(i, j) = 0.0;
				if (H.at<float>(i, j) < 0.0) H.at<float>(i, j) = H.at<float>(i, j) + 360.0;
			}
		}
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				RH.at<uchar>(i, j) = H.at<float>(i, j) *255.0 / 360.0;
				RV.at<uchar>(i, j) = V.at<float>(i, j) *255.0;
				RS.at<uchar>(i, j) = S.at<float>(i, j) *255.0;
				Vec3b val;
				val[0] = RH.at<uchar>(i, j);
				val[1] = RS.at<uchar>(i, j);
				val[2] = RV.at<uchar>(i, j);
				dst.at<Vec3b>(i, j) = val;
			}
		imshow("image", src);
		imshow("Hue", RH);
		imshow("Saturation", RS);
		imshow("Value", RV);
		imshow("dst", dst);
		waitKey();
	}


}

bool inside(int ok1, int ok2) {
	

	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src;

		src = imread(fname, CV_LOAD_IMAGE_COLOR);
		if (ok1>=0 && ok1 < src.rows &&  ok2 < src.cols && ok2>=0) {
			return true;
		}
		else {
			return false;
		}
	}
	getchar();
	getchar();
}

void square(){
	Mat dst(256, 256, CV_8UC3);
	for (int i = 0; i < 128; i++)
	{
		for (int j = 0; j < 128; j++)
		{
			dst.at<Vec3b>(i, j)[0] = 255;
			dst.at<Vec3b>(i, j)[1] = 255;
			dst.at<Vec3b>(i, j)[2] = 255;
			dst.at<Vec3b>(i, j+128)[0] = 0;
			dst.at<Vec3b>(i, j+128)[1] = 0;
			dst.at<Vec3b>(i, j+128)[2] = 255;
			dst.at<Vec3b>(i+128, j)[0] = 0;
			dst.at<Vec3b>(i+128, j)[1] = 255;
			dst.at<Vec3b>(i+128, j)[2] = 0;
			dst.at<Vec3b>(i + 128, j + 128)[0] = 255;
			dst.at<Vec3b>(i + 128, j + 128)[1] = 0;
			dst.at<Vec3b>(i + 128, j + 128)[2] = 0;
		}
	}
	imshow("Destinatie", dst);
	waitKey();
}

void square1(){
	Mat dst(256, 256, CV_8UC3);
	for (int i = 0; i < 50; i++)
	{
		for (int j = 0; j < 50; j++)
		{
			dst.at<Vec3b>(i, j)[0] = 0;
			dst.at<Vec3b>(i, j)[1] = 255;
			dst.at<Vec3b>(i, j)[2] = 255;
		}
	}
	imshow("Destinatie", dst);
	waitKey();
}

void cloneSquare()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_COLOR);
		Mat dst(src.rows, src.cols, CV_8UC3);
		Vec3b pixel1;
		pixel1[0] = 139;
		pixel1[1] = 0;
		pixel1[2] = 139;
		for (int i = 0; i < src.rows; i++)
		{
			for (int j = 0; j < src.cols; j++)
			{
					Vec3b pixel = src.at<Vec3b>(i, j);
					dst.at<Vec3b>(i, j) = pixel;
			}
		}

		for (int i = src.rows / 2 - 50; i < src.rows / 2 + 50; i++){
			for (int j = src.cols / 2 - 50; j < src.cols / 2 + 50; j++){
				dst.at<Vec3b>(i, j) = pixel1;
			}
		}
		imshow("SURSA", src);
		imshow("Destinatie", dst);
		waitKey();
	}
}

void reverse()
{
	float vals[9] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};
	Mat a(3, 3, CV_32FC1, vals);
	cout<<a<<endl;
	Mat b = a.inv();
	cout << "Inversa:" << endl;
	cout << b << endl;
	getchar();
	getchar();
}

void testOpenImagesFld()
{
	char folderName[MAX_PATH];
	if (openFolderDlg(folderName)==0)
		return;
	char fname[MAX_PATH];
	FileGetter fg(folderName,"bmp");
	while(fg.getNextAbsFile(fname))
	{
		Mat src;
		src = imread(fname);
		imshow(fg.getFoundFileName(),src);
		if (waitKey()==27) //ESC pressed
			break;
	}
}

void testResize()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		Mat dst1,dst2;
		//without interpolation
		resizeImg(src,dst1,320,false);
		//with interpolation
		resizeImg(src,dst2,320,true);
		imshow("input image",src);
		imshow("resized image (without interpolation)",dst1);
		imshow("resized image (with interpolation)",dst2);
		waitKey();
	}
}

void testVideoSequence()
{
	VideoCapture cap("Videos/rubic.avi"); // off-line video from file
	//VideoCapture cap(0);	// live video from web cam
	if (!cap.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey(0);
		return;
	}
		
	Mat edges;
	Mat frame;
	char c;

	while (cap.read(frame))
	{
		Mat grayFrame;
		cvtColor(frame, grayFrame, CV_BGR2GRAY);
		imshow("source", frame);
		imshow("gray", grayFrame);
		c = cvWaitKey(0);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished\n"); 
			break;  //ESC pressed
		};
	}
}


void testSnap()
{
	VideoCapture cap(0); // open the deafult camera (i.e. the built in web cam)
	if (!cap.isOpened()) // openenig the video device failed
	{
		printf("Cannot open video capture device.\n");
		return;
	}

	Mat frame;
	char numberStr[256];
	char fileName[256];
	
	// video resolution
	Size capS = Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),
		(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));

	// Display window
	const char* WIN_SRC = "Src"; //window for the source frame
	namedWindow(WIN_SRC, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_SRC, 0, 0);

	const char* WIN_DST = "Snapped"; //window for showing the snapped frame
	namedWindow(WIN_DST, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_DST, capS.width + 10, 0);

	char c;
	int frameNum = -1;
	int frameCount = 0;

	for (;;)
	{
		cap >> frame; // get a new frame from camera
		if (frame.empty())
		{
			printf("End of the video file\n");
			break;
		}

		++frameNum;
		
		imshow(WIN_SRC, frame);

		c = cvWaitKey(10);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished");
			break;  //ESC pressed
		}
		if (c == 115){ //'s' pressed - snapp the image to a file
			frameCount++;
			fileName[0] = NULL;
			sprintf(numberStr, "%d", frameCount);
			strcat(fileName, "Images/A");
			strcat(fileName, numberStr);
			strcat(fileName, ".bmp");
			bool bSuccess = imwrite(fileName, frame);
			if (!bSuccess) 
			{
				printf("Error writing the snapped image\n");
			}
			else
				imshow(WIN_DST, frame);
		}
	}

}

void MyCallBackFunc(int event, int x, int y, int flags, void* param)
{
	//More examples: http://opencvexamples.blogspot.com/2014/01/detect-mouse-clicks-and-moves-on-image.html
	Mat* src = (Mat*)param;
	if (event == CV_EVENT_LBUTTONDOWN)
		{
			printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
				x, y,
				(int)(*src).at<Vec3b>(y, x)[2],
				(int)(*src).at<Vec3b>(y, x)[1],
				(int)(*src).at<Vec3b>(y, x)[0]);
		}
}

void procesareCallBack(int event, int x, int y, int flags, void* param){
	Mat* src = (Mat*)param;

	if (event == CV_EVENT_LBUTTONDBLCLK)
	{
		int perimetru = 0;
		int aria = 0;
		int ariaR = 0;
		int ariaC = 0;
		int p1 = 0;
		int p2 = 0;

		Mat dst((*src).rows, (*src).cols, CV_8UC3);

		for (int i = 0; i < (*src).rows; i++)
			for (int j = 0; j < (*src).cols; j++)
			{
				Vec3b pixel = (*src).at<Vec3b>(i, j);
				if (pixel == (*src).at<Vec3b>(y, x))
				{
					aria++;
					ariaR += i;
					ariaC += j;
				
				}
			}
		printf("%d\n", aria);
		ariaR /= aria;
		ariaC /= aria;
		printf("ri = %f \n", (float)ariaR);
		printf("ci = %f \n", (float)ariaC);

		for (int i = 0; i < (*src).rows; i++)
			for (int j = 0; j < (*src).cols; j++)
			{
				Vec3b pixel = (*src).at<Vec3b>(i, j);
				if (pixel == (*src).at<Vec3b>(y, x))
				{
					p1 += (i - ariaR)*(j - ariaC);
					p2 += (j - ariaC)*(j - ariaC) - (i - ariaR)*(i - ariaR);

				}
			}


		circle(*src, Point(ariaC, ariaR), 3, cv::Scalar(0, 0, 255));
		

		float fi = (atan2(p1, p2)) / 2;

		printf("g = %f \n", (float)fi*180 / 3.14);

		Vec3b black;
		black[0] = black[1] = black[2] = 0;
		for (int i = 1; i < (*src).rows-1; i++)
			for (int j = 1; j < (*src).cols-1; j++){
				if ((*src).at<Vec3b>(y, x) == (*src).at<Vec3b>(i, j)){
					int cnt = 0;
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i - 1, j - 1))
					{
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i - 1, j))
					{
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i - 1, j + 1))
					{
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i, j - 1)){
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i, j + 1)){
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i + 1, j - 1))
					{
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i + 1, j)){
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if ((*src).at<Vec3b>(y, x) != (*src).at<Vec3b>(i + 1, j + 1)){
						dst.at<Vec3b>(i, j) = black;
						cnt++;
					}
					if (cnt > 0)
					perimetru++;
					else
					{
						dst.at<Vec3b>(i, j) = (*src).at<Vec3b>(i, j);
					}

				}
				else
				{
					dst.at<Vec3b>(i, j) = (*src).at<Vec3b>(i, j);
				}
			}



		printf("perimetru = %d\n", perimetru);

		printf("factor de subtiere = %f\n", (4*3.14)*((float)aria/(perimetru*perimetru)));
		imshow("dst", dst);
		imshow("img", *src);
		waitKey();
	}
}

void testMouseClick()
{
	Mat src;
	// Read image from file 
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		//Create a window
		namedWindow("My Window", 1);

		//set the callback function for any mouse event
		setMouseCallback("My Window", MyCallBackFunc, &src);

		//show the image
		imshow("My Window", src);

		// Wait until user press some key
		waitKey(0);
	}
}

void mouseFunction()
{
	Mat src;
	// Read image from file 
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		src = imread(fname);
		//Create a window
		namedWindow("Geometrice", 1);

		//set the callback function for any mouse event
		setMouseCallback("Geometrice", procesareCallBack, &src);

		//show the image
		imshow("Geometrice", src);

		// Wait until user press some key
		waitKey(0);
	}
}

/* Histogram display function - display a histogram using bars (simlilar to L3 / PI)
Input:
name - destination (output) window name
hist - pointer to the vector containing the histogram values
hist_cols - no. of bins (elements) in the histogram = histogram image width
hist_height - height of the histogram image
Call example:
showHistogram ("MyHist", hist_dir, 255, 200);
*/
void showHistogram(const string& name, int* hist, const int  hist_cols, const int hist_height)
{
	Mat imgHist(hist_height, hist_cols, CV_8UC3, CV_RGB(255, 255, 255)); // constructs a white image

	//computes histogram maximum
	int max_hist = 0;
	for (int i = 0; i<hist_cols; i++)
	if (hist[i] > max_hist)
		max_hist = hist[i];
	double scale = 1.0;
	scale = (double)hist_height / max_hist;
	int baseline = hist_height - 1;

	for (int x = 0; x < hist_cols; x++) {
		Point p1 = Point(x, baseline);
		Point p2 = Point(x, baseline - cvRound(hist[x] * scale));
		line(imgHist, p1, p2, CV_RGB(255, 0, 255)); // histogram bins colored in magenta
	}

	imshow(name, imgHist);
}

void fdp()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		int V[256];
		float FDP[256];
		for (int i = 0; i < 256; i++) V[i] = 0;
		Mat src;
		src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				V[val]++;
			}
		for (int i = 0; i < 255; i++)
		{
			FDP[i] = (float)V[i] / (src.rows * src.cols);
		}
		for (int i = 0; i < 255; i++) printf("%.3f ", FDP[i]);
		getchar();
		getchar();

	}

}

void prag()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		int V[256];
		float FDP[256];
		int prag[256];
		prag[0] = 0;
		for (int i = 0; i < 256; i++) V[i] = 0;

		Mat src;
		src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC1);
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				V[val]++;
			}

		for (int i = 0; i < 256; i++)
		{
			FDP[i] = (float)V[i] / (src.rows * src.cols);
		}
		int n = 1;
		int val = 11;
		for (int i = val/2; i < 255-val/2; i++)
		{
			bool OK = true;
			float media = 0.0;
			for (int j = i - val/2; j <= i + val/2; j++)
			{
				media = media + FDP[j];
				if (FDP[i] < FDP[j]) OK = false;
			}
			media = (media / float(val)) + 0.0003;
			if (media > FDP[i]) OK = false;
			if (OK == true) {
				prag[n] = i;
				n++;
			}
		}
		prag[n] = 255;

		for (int i = 0; i <= n; i++) printf("%d ", prag[i]);
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				int k;
				for (k = 0; val>prag[k]; k++);
				if ((val - prag[k - 1]) >(prag[k] - val)) dst.at<uchar>(i, j) = prag[k];
				else dst.at<uchar>(i, j) = prag[k - 1];
			}
		imshow("SRC", src);
		imshow("DST", dst);
		waitKey();
	}
}

void histo()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		int V[256];
		for (int i = 0; i < 256; i++) V[i] = 0;

		Mat src;
		src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				V[val]++;
			}
		showHistogram("Histogram", V, 256, 300);
		imshow("Src", src);
		waitKey();
	}
}

void floyd()
{
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		int V[256];
		float FDP[256];
		int prag[256];
		prag[0] = 0;
		for (int i = 0; i < 256; i++) V[i] = 0;

		Mat src;
		src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst=Mat::zeros(src.rows, src.cols, CV_8UC1);
		
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				V[val]++;
			}
	
		for (int i = 0; i < 256; i++)
		{
			FDP[i] = (float)V[i] / (src.rows * src.cols);
		}
		int n = 1;
		int val = 11;
		for (int i = val / 2; i < 255 - val / 2; i++)
		{
			bool OK = true;
			float media = 0.0;
			for (int j = i - val / 2; j <= i + val / 2; j++)
			{
				media = media + FDP[j];
				if (FDP[i] < FDP[j]) OK = false;
			}
			media = (media / float(val)) + 0.0003;
			if (media > FDP[i]) OK = false;
			if (OK == true) {
				prag[n] = i;
				n++;
			}
		}
		prag[n] = 255;

		for (int i = 0; i <= n; i++) printf("%d ", prag[i]);


		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				dst.at<uchar>(i, j) = src.at<uchar>(i, j);
			
			}
		
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{

				uchar oldpixel = dst.at<uchar>(i, j);
				uchar newpixel = dst.at<uchar>(i, j);
				int k;
				for (k = 0; oldpixel>prag[k]; k++);
				if ((oldpixel - prag[k - 1]) >(prag[k] - oldpixel)) newpixel = prag[k];
				else newpixel = prag[k - 1];
				dst.at<uchar>(i, j) = newpixel;
				
				int err = oldpixel - newpixel;

				if (i >= 0 && i < src.rows &&  j + 1 < src.cols && j + 1 >= 0){
					dst.at<uchar>(i, j+1) += 7 * err / 16;
				}
				if (i + 1 >= 0 && i + 1 < src.rows &&  j-1 < src.cols && j-1 >= 0){
					dst.at<uchar>(i+1, j-1) += 3 * err / 16;
				}
				if (i + 1 >= 0 && i + 1 < src.rows &&  j < src.cols && j >= 0){
					dst.at<uchar>(i+1, j) += 5 * err / 16;
				}
				if (i + 1 >= 0 && i + 1 < src.rows &&  j + 1 < src.cols && j + 1 >= 0){
					dst.at<uchar>(i+1, j+1) += 1 * err / 16;
				}

			}
		
		imshow("SRC", src);
		imshow("DST", dst);
		waitKey();
	}
}

bool isInside(int x, int y, int r, int c){
	if (x > 0 && y > 0 && y < r && x < c)
		return true;
	return false;
}

void etichetare(){
	char fname[MAX_PATH];
	if (openFileDlg(fname))
	{
		int no_eticheta = 0;


		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(src.rows, src.cols, CV_8UC3);
		Mat labels = Mat::zeros(src.rows, src.cols, CV_32SC1);

		int di[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
		int dj[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };


		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				if ((src.at<uchar>(i, j) == 0) && (labels.at<int>(i, j) == 0))
				{
					no_eticheta++;
					std::queue<Point2i> q;
					labels.at<int>(i, j) = no_eticheta;
					q.push(Point(i, j));
					while (!q.empty()){
						Point2i pct = q.front();
						q.pop();
						for (int ii = 0; ii < 8; ii++)
						{
							if (isInside(pct.x + di[ii], pct.y + dj[ii], src.rows, src.rows) && labels.at<int>(pct.x + di[ii], pct.y + dj[ii]) == 0){
								if (src.at<uchar>(pct.x + di[ii], pct.y + dj[ii]) == 0)
								{
									q.push(Point(pct.x + di[ii], pct.y + dj[ii]));
									labels.at<int>(pct.x + di[ii], pct.y + dj[ii]) = no_eticheta;
								}
							}
						}
					}

				}
			}

		vector<Vec3b> colors;
		std::default_random_engine gen;
		std::uniform_int_distribution<int> d(10, 255);

		for (int i = 0; i <= no_eticheta; i++)
		{
			uchar r = d(gen);
			uchar g = d(gen);
			uchar b = d(gen);
			colors.push_back(Vec3b(r, g, b));
		}

		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				if (labels.at<int>(i, j) != 0)
				{

					dst.at<Vec3b>(i, j) = colors[labels.at<int>(i, j)];
				}
			}

		imshow("SRC", src);
		imshow("DST", dst);
		waitKey();
	}
}


typedef struct{
	int x, y;
	int c;
}my_point;

void alg(){
	int prag = 0;
	int dj[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };
	int di[8] = { 0, -1, -1, -1, 0, 1, 1, 1 };
	char fname[MAX_PATH];

	while (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst(img.rows, img.cols, CV_8UC1);
		Mat dst1(img.rows, img.cols, CV_8UC1);
		vector <int> derivata;
		vector <my_point> contur;
		int dir = 7;
		for (int i = 0; i < img.rows; i++)
			for (int j = 0; j < img.cols; j++)
			{
				dst.at<uchar>(i, j) = 255;
				dst1.at<uchar>(i, j) = 255;
			}

		int x_start, y_start;
		bool ok = false;
		for (int i = 0; i < img.rows; i++)
			for (int j = 0; j < img.cols; j++){
				if (img.at<uchar>(i, j) == 0){
					x_start = j;
					y_start = i;
					break;
					ok = true;
				}
			}
		if (ok)
			break;
		contur.push_back(my_point{ x_start, y_start, 7 });
		int n = 0;
		bool finished = false;
		int j = x_start, i = y_start, x, y;
		while (!finished){
			if (dir % 2 == 0)
				dir = (dir + 7) % 8;
			else
				dir = (dir + 6) % 8;

			for (int k = 0; k < 7; k++){
				int d = (dir + k) % 8;
				x = j + dj[d];
				y = i + di[d];

				if (img.at<uchar>(y, x) == 0){
					dir = d;
					contur.push_back(my_point{ x, y, dir });
					dst.at<uchar>(y, x) = 0;
					j = x; i = y;
					n++;
					break;
				}
				else
					dir = (dir + 1) % 8;
			}
			if ((n>1) && (contur[0].x == contur[n - 1].x) && (contur[1].x == contur[n].x) && (contur[0].y == contur[n - 1].y) && (contur[1].y == contur[n].y))
				finished = true;
		}
		printf("Vectorul contur: \n");
		for (int i = 0; i <= n; i++){

			g << contur[i].c << endl;
			printf(" %d ", contur[i].x);
			printf(" %d ", contur[i].y);
			printf("\n");
		}

		for (int i = 0; i < n; i++){
			derivata.push_back((contur[i].c + 7 - contur[i + 1].c) % 8);
			printf(" %d ", (contur[i].c+ 7 - contur[i + 1].c)%8);
		}

	
		int val1 = contur[0].x, val2 = contur[0].y;
		for (int i = 0; i < n; i++){
			dst1.at<uchar>(val1, val2) = 0;
			val1 += di[contur[i].c];
			val2 += dj[contur[i].c];
		}

		imshow("1", img);
		imshow("2", dst);
		imshow("3", dst1);
		waitKey();
	}
}

Mat dilatare(Mat src){
	Mat dst (src.rows, src.cols, CV_8UC1);
	
	for (int i = 1; i < src.rows-1; i++)
		for (int j = 1; j < src.cols-1; j++)
		{
			if (src.at<uchar>(i, j) == 0)
			{
				for (int k = 0; k < 8; k++)
					dst.at<uchar>(i + dib[k], j + djb[k]) = 0;
			}
			else
			{
				dst.at<uchar>(i, j) = 255;
			}
		}
	return dst;
}

Mat eroziune(Mat src){
	Mat dst(src.rows, src.cols, CV_8UC1);
	for (int i = 1; i < src.rows-1; i++)
		for (int j = 1; j < src.cols-1; j++)
		{
			if (src.at<uchar>(i, j) == 0)
			{
				bool ok = true;
				for (int k = 0; k < 8; k++)
					if (src.at<uchar>(i + dib[k], j + djb[k]) != 0)
						ok = false;
				if (ok)
					dst.at<uchar>(i, j) = 0;
				else
					dst.at<uchar>(i, j) = 255;
			}
			else
			{
				dst.at<uchar>(i, j) = 255;
			}
		}
	return dst;
}

void eroziune_fct(){
	char fname[MAX_PATH];
	if (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = eroziune(img);
		imshow("src", img);
		imshow("dst", dst);
		waitKey();
	}
}

void dilatare_fct(){
	char fname[MAX_PATH];
	if (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = dilatare(img);
		imshow("src", img);
		imshow("dst", dst);
		waitKey();
	}
}

void deschidere(){
	char fname[MAX_PATH];
	if (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = eroziune(img);
		Mat dst1 = dilatare(dst);
		imshow("src", img);
		imshow("dst1", dst1);
		waitKey();
	}
}

void inchidere(){
	char fname[MAX_PATH];
	if (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = dilatare(img);
		Mat dst1 = eroziune(dst);
		imshow("src", img);
		imshow("dst1", dst1);
		waitKey();
	}
}

void extragere_contur(){
	char fname[MAX_PATH];
	if (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = eroziune(img);
		Mat dst1(img.rows, img.cols, CV_8UC1);
		for (int i = 0; i < img.rows; i++)
			for (int j = 0; j < img.cols; j++){
				dst1.at<uchar>(i, j) = img.at<uchar>(i, j) + 255 - dst.at<uchar>(i, j);

			}
		imshow("src", img);
		imshow("dst1", dst1);
		waitKey();
	}
}

Mat negat(Mat src){
	Mat dst(src.rows, src.cols, CV_8UC1);
	for (int i = 1; i < src.rows - 1; i++)
		for (int j = 1; j < src.cols - 1; j++)
		{
			if (src.at<uchar>(i, j) == 0){
				dst.at<uchar>(i, j) = 255;
			}
			else
			{
				dst.at<uchar>(i, j) = 0;
			}
		}
	return dst;
}

bool egale(Mat x1, Mat x2){
	for (int i = 0; i < x1.rows; i++)
		for (int j = 0; j < x1.cols; j++)
			if (x1.at<uchar>(i, j) != x1.at<uchar>(i, j))
				return false;
	return true;
}

Mat intersectare(Mat x1, Mat x2){
	Mat dst(x1.rows, x1.cols, CV_8UC1);
	for (int i = 0; i < x1.rows; i++)
		for (int j = 0; j < x1.cols; j++)
			if (x1.at<uchar>(i, j) == x1.at<uchar>(i, j))
				dst.at<uchar>(i, j) = x1.at<uchar>(i, j);
			else
			{
				dst.at<uchar>(i, j) = 255;
			}
	return dst;
}

Mat uniune(Mat x1, Mat x2){
	Mat dst(x1.rows, x1.cols, CV_8UC1);
	for (int i = 0; i < x1.rows; i++)
		for (int j = 0; j < x1.cols; j++)
			if (x1.at<uchar>(i, j) == 0 || x1.at<uchar>(i, j) == 0)
				dst.at<uchar>(i, j) = 0;
			else
			{
				dst.at<uchar>(i, j) = 255;
			}
	return dst;
}

void umplere(){
	char fname[MAX_PATH];
	if (openFileDlg(fname)){
		Mat img = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = negat(img);
		Mat x0(img.rows, img.cols, CV_8UC1);
		for (int i = 0; i < img.rows; i++)
			for (int j = 0; j < img.cols; j++)
				x0.at<uchar>(i, j) = 255;
		x0.at<uchar>(50, 50) = 0;
		Mat x1 = intersectare(dilatare(x0), dst);
		while (! egale(x0, x1)){
			x0 = x1;
			x1 = intersectare(dilatare(x0), dst);
		}

		Mat dstfinal = uniune(x0, img);
		imshow("src", img);
		imshow("dst1", dstfinal);
		waitKey();
	}
}

float media(Mat src) {
	int V[256];
	float fdp[256];
	float medie = 0.0;
	for (int i = 0; i < 256; i++) V[i] = 0;
	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			uchar val = src.at<uchar>(i, j);
			V[val]++;
		}

	}
	for (int i = 0; i < 256; i++) {
		medie += 1.0*i*V[i] / (src.rows* src.cols);
	}
	return medie;
}

void deviatia() {
	char fname5[MAX_PATH];
	if (openFileDlg(fname5))
	{
		Mat src;
		src = imread(fname5, CV_LOAD_IMAGE_GRAYSCALE);
		float m = media(src);
		printf("media: %f\n", m);
		float suma = 0.0;
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.rows; j++)
			{
				suma += ((src.at<uchar>(i, j) - m) * (src.at<uchar>(i, j) - m));
			}
		suma = suma / (src.rows*src.cols);
		suma = sqrt(suma);
		printf("deviatia: %f\n", suma);
		getchar();
		getchar();
	}
}

void histo1(Mat src, char *nume_fereastra)
{

	Mat dst(256, 256, CV_8UC1);
	int V[256];
	for (int i = 0; i < 256; i++) V[i] = 0;


	for (int i = 0; i < src.rows; i++)
		for (int j = 0; j < src.cols; j++)
		{
			uchar val = src.at<uchar>(i, j);
			V[val]++;
		}
	showHistogram(nume_fereastra, V, 256, 300);

}

void contrast() {
	char fname5[MAX_PATH];
	if (openFileDlg(fname5))
	{
		Mat src;
		src = imread(fname5, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = Mat(src.rows, src.cols, CV_8UC1);
		int goutMin = 100;
		int goutMax = 200;
		int imgMin = 255;
		int imgMax = 0;
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar value = src.at<uchar>(i, j);
				if (value < imgMin)imgMin = value;
				if (value > imgMax)imgMax = value;
			}

		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				dst.at<uchar>(i, j) = ((src.at<uchar>(i, j) - imgMin)  * (goutMax - goutMin)) / (imgMax - imgMin) + goutMin;
			}
		imshow("src", src);
		imshow("dst", dst);
		histo1(dst, "histo_dst");
		histo1(src, "histo_src");
		waitKey();
	}
}

void gamma() {
	char fname7[MAX_PATH];
	if (openFileDlg(fname7))
	{
		Mat src;
		src = imread(fname7, CV_LOAD_IMAGE_GRAYSCALE);
		Mat dst = Mat(src.rows, src.cols, CV_8UC1);
		int g = 0.1;
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				dst.at<uchar>(i, j) = (255 * pow((float)val / 255, g));
			}
		imshow("src", src);
		imshow("dst", dst);
		waitKey();
	}
}

void binarizare() {
	char fname8[MAX_PATH];
	if (openFileDlg(fname8))
	{
		Mat src;
		src = imread(fname8, CV_LOAD_IMAGE_GRAYSCALE);
		int err = 1;
		Mat dst = Mat(src.rows, src.cols, CV_8UC1);
		int V[256];
		for (int i = 0; i < 256; i++) V[i] = 0;


		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				V[val]++;
			}


		int imgMin = 255;
		int imgMax = 0;
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar value = src.at<uchar>(i, j);
				if (value < imgMin)imgMin = value;
				if (value > imgMax)imgMax = value;
			}
		int T = 165;
		int T1 = 165;
		do
		{
			T = T1;
			int g = 0;
			int l = 0;
			int ng = 0;
			int nl = 0;
			for (int i = 0; i < 256; i++)
			{
				if (V[i] > T)
				{
					g += i * V[i];
					ng += V[i];
				}
				else
				{
					l += i * V[i];
					nl += V[i];
				}
			}
			g = g / ng;
			l = l / nl;
			T1 = g + l;

		} while (abs(T1 - T) < err);

		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				if (src.at<uchar>(i, j) < T)dst.at<uchar>(i, j) = 0;
				else dst.at<uchar>(i, j) = 255;
			}

		imshow("src", src);
		imshow("dst", dst);
		waitKey();
		waitKey();
	}
}

void egal_hist() {
	char fname9[MAX_PATH];
	if (openFileDlg(fname9))
	{
		Mat src;
		src = imread(fname9, CV_LOAD_IMAGE_GRAYSCALE);

		Mat dst = Mat(src.rows, src.cols, CV_8UC1);
		int V[256];
		for (int i = 0; i < 256; i++) V[i] = 0;


		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				uchar val = src.at<uchar>(i, j);
				V[val]++;
			}
		float pc[256];
		for (int k = 0; k < 256; k++)
		{
			int sum = 0;
			for (int kk = 0; kk <= k; kk++)sum += V[kk];
			float x = (float)sum / (src.rows * src.cols);
			pc[k] = x;
		}
		for (int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++)
			{
				dst.at<uchar>(i, j) = 255 * pc[src.at<uchar>(i, j)];
			}
		imshow("src", src);
		imshow("dst", dst);
		histo1(dst, "histo_dst");
		histo1(src, "histo_src");
		waitKey();
	}
}

int main()
{
	int op;
	int th;
	do
	{
		system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 1 - Open image\n");
		printf(" 2 - Open BMP images from folder\n");
		printf(" 3 - Resize image\n");
		printf(" 4 - Process video\n");
		printf(" 5 - Snap frame from live video\n");
		printf(" 6 - Mouse callback demo\n");
		printf(" 7 - Clone and Add\n");
		printf(" 8 - Clone and Sub\n");
		printf(" 9 - Clone and Mul\n");
		printf(" 10 - Clone and Negative\n");
		printf(" 11 - Clone and Add Color\n");
		printf(" 12 - Clone and Sub Color\n");
		printf(" 13 - Clone Mono\n");
		printf(" 14 - Square\n");
		printf(" 15 - Clone Square\n");
		printf(" 16 - Yellow Square\n");
		printf(" 17 - Reverse\n");
		printf(" 18 - 3 Grayscale\n");
		printf(" 19 - RGB to Grayscale\n");
		printf(" 20 - Black and white\n");
		printf(" 21 - RGB to USV\n");
		printf(" 22 - RGB to Grayscale Ponder\n");
		printf(" 23 - Is inside\n");
		printf(" 24 - HSV\n");
		printf(" 25 - Histograma\n");
		printf(" 26 - FDP\n");
		printf(" 27 - Praguri\n");
		printf(" 28 - Floyd\n");
		printf(" 29 - Mouse\n");
		printf(" 30 - Etichetare\n");
		printf(" 31 - Border\n");
		printf(" 32 - Dilatare\n");
		printf(" 33 - Erodare\n");
		printf(" 34 - Deschidere\n");
		printf(" 35 - Inchidere\n");
		printf(" 36 - Extragere contur\n");
		printf(" 37 - Umplere regiuni\n");
		printf(" 38 - deviatie\n");
		printf(" 39 - contrast\n");
		printf(" 40 - gamma\n");
		printf(" 41 - binarizare\n");
		printf(" 42 - egalizareHist\n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");
		scanf("%d",&op);
		switch (op)
		{
			case 1:
				testOpenImage();
				break;
			case 2:
				testOpenImagesFld();
				break;
			case 3:
				testResize();
				break;
			case 4:
				testVideoSequence();
				break;
			case 5:
				testSnap();
				break;
			case 6:
				testMouseClick();
				break;
			case 7:
				cloneAdd();
				break;
			case 8:
				cloneSub();
				break;
			case 9:
				cloneMul();
				break;
			case 10:
				cloneNegative();
				break;
			case 11:
				cloneAddC();
				break;
			case 12:
				cloneSubC();
				break;
			case 13:
				cloneMono();
				break;
			case 14:
				square();
				break;
			case 15:
				cloneSquare();
				break;
			case 16:
				square1();
				break;
			case 17:
				reverse();
				break;
			case 18:
				clone2Grayscale();
				break;
			case 19:
				rgb2gs();
				break;
			case 20:
				scanf("%d", &th);
				cloneTh(th);
				break;
			case 21:
				rgb2usv();
				break;
			case 22:
				rgb2gsPonder();
				break;
			case 23:
			{
				scanf("%d", &ok1);
				scanf("%d", &ok2);
				if (inside(ok1, ok2))
				{ 
					printf("true"); 
				}
				else
				{
					printf("false");
				}
			}
				break;
			case 24:
				rgb2usv();
				break;
			case 25:
				histo();
				break;
			case 26:
				fdp();
				break;
			case 27:
				prag();
				break;
			case 28:
				floyd();
				break;
			case 29:
				mouseFunction();
				break;
			case 30 :
				etichetare();
				break;
			case 31:
				alg();
				break;
			case 32:
				dilatare_fct();
				break;
			case 33:
				eroziune_fct();
				break;
			case 34:
				deschidere();
				break;
			case 35:
				inchidere();
				break;
			case 36:
				extragere_contur();
				break;
			case 37:
				umplere();
				break;
			case 38:
				deviatia();
				break;
			case 39:
				contrast();
				break;
			case 40:
				gamma();
				break;
			case 41:
				binarizare();
				break;
			case 42:
				egal_hist();
				break;
		}
	}
	while (op!=0);
	return 0;
}